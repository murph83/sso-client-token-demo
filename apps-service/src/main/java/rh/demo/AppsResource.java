package rh.demo;

import io.quarkus.security.Authenticated;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/apps")
public class AppsResource {

    @Inject
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<App> getApps(){
        return em.createQuery("SELECT a FROM App a", App.class).getResultList();
    }
}