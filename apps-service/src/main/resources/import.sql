INSERT INTO APP(id, name, url, description) VALUES (1, 'Fisheries', 'http://fisheries.apps-crc.testing', 'An app for Fish');
INSERT INTO APP(id, name, url, description) VALUES (2, 'AvianCheck', 'http://aviancheck.apps-crc.testing', 'An app for Birds');
INSERT INTO APP(id, name, url, description) VALUES (3, 'Brexit', 'http://brexit.apps-crc.testing', 'An app for Leaving');