import React from "react";
import {
    Card,
    CardBody,
    CardHead,
    Split,
    SplitItem,
    Text,
    TextContent,
    TextVariants,
    Title
} from "@patternfly/react-core";
import {ApplicationsIcon} from "@patternfly/react-icons";
import "./AppCard.css";

export interface App {
    id: number
    name: string
    description: string
    url?: string
}

type OptionalLinkProps = {
    url?: string
}

const OptionalLink: React.FC<OptionalLinkProps> = ({url, children }) => {
    return (
        url ?
            <a href={url}>{children}</a>
            :
            <React.Fragment>{children}</React.Fragment>
    );
};


export const AppCard: React.FC<App> = ({id, name, description, url}: App) => {
    return (
        <OptionalLink url={url}>
            <Card isHoverable style={{minHeight: '15rem'}}>
                <CardHead>
                    <Split gutter={"md"}>
                        <SplitItem>
                            <ApplicationsIcon size={"md"}/>
                        </SplitItem>
                        <SplitItem>
                            <Title size={"md"}>{name}</Title>
                        </SplitItem>
                    </Split>
                </CardHead>
                <CardBody>
                    <TextContent>
                        <Text component={TextVariants.p}>
                            {description}
                        </Text>
                    </TextContent>
                </CardBody>
            </Card>
        </OptionalLink>
    );
};