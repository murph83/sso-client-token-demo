import React from "react";
import KeycloakProvider from "./KeycloakProvider";
import AppGallery from "./AppGallery";
import Keycloak, {KeycloakInstance} from "keycloak";

const SecureGallery: React.FC = () => {
    const keycloak: KeycloakInstance = Keycloak({
        url: 'https://sso-dafm.apps-crc.testing/auth',
        realm: 'ApplicationRealm',
        clientId: 'apps-launcher'
    });

    keycloak.init({
        onLoad: 'login-required'
    });

  return (
      <KeycloakProvider keycloak={keycloak}>
        <AppGallery/>
      </KeycloakProvider>
  );
};

export default SecureGallery;