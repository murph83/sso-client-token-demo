import React from 'react';
import ReactDOM from 'react-dom';
//import AppGallery from './AppGallery';
import SecureGallery from "./SecureGallery";

ReactDOM.render(<SecureGallery />, document.getElementById('root'));
