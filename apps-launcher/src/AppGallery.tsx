import React, {useContext, useEffect, useState} from 'react';
import '@patternfly/react-core/dist/styles/base.css';
import {Gallery, GalleryItem, Page, PageSection} from "@patternfly/react-core";
import {App, AppCard} from "./AppCard";
import './AppGallery.css';
import {KeycloakContext} from "./KeycloakProvider";

const AppGallery: React.FC = () => {
    const [ apps, setApps ] = useState<App[] | undefined>(
        [
            {id: 1, name:"Fisheries", url: "http://localhost:5000", description: "Description"},
            {id: 2, name:"AvianCheck", url: "http://localhost:5000", description: "Description"},
            {id: 3, name:"Brexit", url: "http://localhost:5000", description: "Description"}
        ]
    );

    const kc = useContext(KeycloakContext);

    useEffect(() => {
        fetch("http://localhost:8080/apps", {mode: 'cors', headers: {'Authorization': 'Bearer: ' + (kc && kc.token)}})
            .then(response => {
                if (!response.ok) { throw response }
                return response.json()
            })
            .then(setApps);
    }, [kc]);

  return (
      <Page>
          <PageSection>
              <Gallery gutter={"sm"}>
                  { apps && apps.map(app => (
                      <GalleryItem key={app.id}>
                          <AppCard {...app} />
                      </GalleryItem>
                    ))
                  }
              </Gallery>
          </PageSection>
      </Page>
  );
};

export default AppGallery;
