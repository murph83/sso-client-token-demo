import React, {useContext} from "react";
import {KeycloakInstance} from "keycloak/keycloak";

export const KeycloakContext = React.createContext<KeycloakInstance|undefined>(undefined);

type KeycloakProviderProps = {
    keycloak?: KeycloakInstance;
}

const KeycloakProvider: React.FC<KeycloakProviderProps> = ({keycloak, children}) => {

    return (
        <KeycloakContext.Provider value={keycloak}>
            {children}
        </KeycloakContext.Provider>
    );
};

export default KeycloakProvider;