# Comprehensive Authentication and Authoriztion with Keycloak

When securing applications, there are a lot of things to consider. Often, there is a legacy estate of authentication/authorization (authn/z) services to contend with, as well as requests to allow integrations with newer systems. There are internal and external clients, and innumerable development platforms and technologies. To address this, you need a flexible, standards-based platform for delivering SSO capabilities. 

Enter RH-SSO. Based on the Keycloak project, it delivers a stable platform on the OpenID Connect standard, and allows for the wide variety of access control needs.

# Client Types
Clients are divided into two types: Clients that can request a login, and clients that cannot. 
Clients that can request a login can be either Public or Confidential. This refers to whether they are able to keep a set of client credentials secret or not. Most Single-Page applications and Client-only webapps are public, since there is no way to keep client credentials secret in this architecture. This is not a problem, but it requires that the valid redirect URLs be tightly specified, so that SSO will only redirect back to known application URLs. Use of public clients also
prevents CSRF attacks, which apply primarily to cookie-based authentication systems.

Confidential clients are able to keep a secret secure. This can include client-server models that store the authentication token in session storage, as well as services endpoints that can issue redirects to the sso endpoint.

# Identity and Access Control Maturity Model

## Phase 1: Unified Authentication
This is directly delivered by using RH-SSO as a federating Identity Provider. New applications wishing to [integrate](#Integrations) with SSO, or old applications wishing to [migrate](#Migrations), can connect directly with RH-SSO and participate in the same realm of trust that currently exists in the organization.


## Phase 2: Coarse-Grained Access Control
Role based access control (RBAC) remains the gold-standard for high-level access. Domain-specific permissions are grouped together to allow logical administration of access to collectiosn of similar users. While in large systems it is possible to have complex sets of roles, it is important to remember that the roles are not permissions themselves. These are either implemented directly in code, or enhanced further via [Fine-grained access control](#phase-3-fine-grained-access-control)

In OpenID Connect, roles are mapped into the auth_token as claims. By appropriately managing claim scope, following the principle of least privilege, clients receive only the roles they require for their current activity. When moving between scopes, users are assigned a new auth_token appropriate to their new context. When using RH-SSO, this process is completely transparent to both the user and the clients.


## Phase 3: Fine-Grained Access Control
Once the complexity of the systems permission model moves beyond a certain point, it is helpful to externalize this capability. Often, this leads to bespoke systems for managing, recalling, and enforcing permissions policies. RH-SSO includes a facility to deliver this capability via the OIDC standard. The architecture allows for pluggable policy definition and decisioning, while making enforcement easy by building on the same standard that is used to deliver the rest of the IAM flow. This simplifies adoption, and allows clients to benefit from the enhanced detail in their access model without requiring additional development work.

At its simplest, a permission model is      

X CAN DO Y ON RESOURCE Z

where

+ X represents one or more users, roles, or groups, or a combination of them. You can also use claims and context here.
+ Y represents an action to be performed, for example, `write`, `view`, and so on.
+ Z represents a protected resource, for example, `/accounts`. 

Use of the RH-SSO Authorization Services is completely optional, but there are a number of core concepts that should be applied to any system aiming to add fine-grained control. The result is usually something like this:
![Authz Architecture](images/authz-arch-overview.png)

In order to completely implement this kind of system, an number of well-established pieces are needed. RH-SSO makes adoption of this standard easier for end-users by implementing Policy Administration, Decisioning, Enforcement, and Information APIs, and making them consumable through the same authorization mechanism already in place in OIDC.
For more detail, see the [docs](https://access.redhat.com/documentation/en-us/red_hat_single_sign-on/7.3/html/authorization_services_guide/overview#overview_architecture)


# Integrations



# Migrations



# Examples
## Understanding the sample ApplicationRealm
The sample Realm consists of one IDP (Red Hat SSO), four clients, and a resource server. Technology is demonstrated using the following mapping:

| project                        | client-id     | View Framework | Server Framework |
| -------                        | ---------     | -------------- | ---------------- |
| [apps-launcher](apps-launcher) | apps-launcher | React          | Express          |
| [claims](claims)               | claims        | React          | Spring Boot      |
| [angler-app](angler-app)       | angler-app    | Angular        | N/A              |
| [petstore](petstore)           | petstore      | JSF            | JEE              |
| [inventory](inventory)         | inventory     | N/A            | Spring Boot      |

### Moving between two client-server applications
### Moving from a client-server application to an SPA and back
### Integrating with legacy frameworks
### Managing access to resource servers
### Adding a new backend to a client
