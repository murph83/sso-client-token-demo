import express, {Request, Response, NextFunction} from 'express';
import session from 'express-session';
import path from 'path';
import request from 'request';
import * as util from "util";

// js imports
let Keycloak = require('keycloak-connect');

// init express
const app = express();
app.set("port", process.env.PORT || 3000);

// init keycloak
let memoryStore = new session.MemoryStore();
let keycloak = new Keycloak({ store: memoryStore });
app.use(session({
    secret: 'changeme!',
    resave: false,
    saveUninitialized: true,
    store: memoryStore
}));


const dumpToken = (req: Request, res: Response, next: NextFunction) => {
    console.log(util.inspect(req, {showHidden: false, depth: null}));
    next()
};


app.use(keycloak.middleware());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.get('/api/:service', keycloak.protect(), (req:Request, res:Response) => {
    console.log(JSON.parse(req.session['keycloak-token']).access_token)
    request('http://localhost:8081/' + req.params['service'], {
        'auth': {
            'bearer': JSON.parse(req.session['keycloak-token']).access_token
        }
    },(err, body) => {
        res.json(body);
    });
});
app.use(keycloak.protect(), dumpToken, express.static(path.join(__dirname, '../frontend/build')));


const server = app.listen(app.get("port"), () => {
    console.log(
        "  App is running at http://localhost:%d in %s mode",
        app.get("port"),
        app.get("env")
    );
    console.log("  Press CTRL-C to stop\n");
});


export default server;
