import React from 'react';
import ReactDOM from 'react-dom';
import AppLauncher from './AppLauncher';

ReactDOM.render(<AppLauncher />, document.getElementById('root'));
