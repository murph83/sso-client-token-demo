import React from 'react';
import ReactDOM from 'react-dom';
import AppLauncher from './AppLauncher';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AppLauncher />, div);
  ReactDOM.unmountComponentAtNode(div);
});
