import React from 'react';
import '@patternfly/react-core/dist/styles/base.css';
import {
  Page,
  PageSection
} from "@patternfly/react-core";
import './AppLauncher.css';


const AppLauncher: React.FC = () => {
  return (
      <Page>
        <PageSection>
          Department of Motor Vehicles
        </PageSection>
      </Page>
  );
}

export default AppLauncher;
