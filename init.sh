#!/bin/sh

# Install ansible dependencies
ansible-galaxy install -r .ansible/requirements.yml --roles-path .ansible/roles

# Execute init playbook
ansible-playbook -i .ansible/hosts .ansible/init.yml
